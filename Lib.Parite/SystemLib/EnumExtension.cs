﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.Pirate.CSharp.SystemLib
{
    /// <summary>
    /// 枚举类型扩展
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EnumExtension<T>
            where T : struct
    {
        public EnumExtension()
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("传入的类型不是枚举");
            }
        }

        /// <summary>
        /// 获取枚举类型的所有成员
        /// </summary>
        /// <returns></returns>
        public List<T> GetMembers()
        {
            List<T> rev = new List<T>();
            foreach (var item in Enum.GetNames(typeof(T)))
            {
                T t;
                Enum.TryParse<T>(item, out t);
                rev.Add(t);
            }

            return rev;
        }

        /// <summary>
        /// 解压缩，仅当枚举的值是按“位”定义的时候可用。
        /// 例如enum ETest{ a = 1, b = 2, c = 4};
        /// 则当ezip=ETest.a | ETest.b | ETest.c时，可返回三者的数组。
        /// </summary>
        /// <param name="ezip"></param>
        /// <returns></returns>
        public List<T> Decompression(T ezip)
        {
            var members = GetMembers();
            return EnumExtension.Decompression(ezip, members);
        }

        /// <summary>
        /// 压缩，即 | 操作。若未传入es，或其长度为0，则压缩整个枚举
        /// </summary>
        /// <param name="es"></param>
        /// <returns></returns>
        public T Compression(params T[] es)
        {
            if (null == es || 0 == es.Length)
                es = this.GetMembers().ToArray();

            int revHash = 0;
            es.ToLookup(q => revHash = (revHash | q.GetHashCode()));

            T rev;
            Enum.TryParse<T>(revHash.ToString(), out rev);

            return rev;
        }
    }

    public class EnumExtension
    {
        /// <summary>
        /// 获取枚举类型的所有成员
        /// </summary>
        /// <returns></returns>
        public static List<TE> GetMembers<TE>()
            where TE : struct
        {
            var rev = new EnumExtension<TE>().GetMembers();
            return rev;
        }

        /// <summary>
        /// 解压缩，仅当枚举的值是按“位”定义的时候可用。
        /// 例如enum ETest{ a = 1, b = 2, c = 4};
        /// 则当ezip=ETest.a | ETest.b | ETest.c时，可返回三者的数组。
        /// </summary>
        /// <param name="ezip"></param>
        /// <returns></returns>
        public static List<TE> Decompression<TE>(TE ezip, IEnumerable<TE> members)
            where TE : struct
        {
            if (!typeof(TE).IsEnum)
            {
                throw new ArgumentException("传入的类型不是枚举");
            }

            List<TE> rev = new List<TE>();
            foreach (var item in members)
            {
                int iVal = item.GetHashCode();
                if (iVal == (ezip.GetHashCode() & iVal))
                {
                    rev.Add(item);
                }
            }
            return rev;
        }

        /// <summary>
        /// 压缩，即 | 操作。若未传入es，或其长度为0，则压缩整个枚举
        /// </summary>
        /// <param name="es"></param>
        /// <returns></returns>
        public static TE Compression<TE>(params TE[] es)
            where TE : struct
        {
            var rev = new EnumExtension<TE>().Compression(es);
            return rev;
        }
    }
}

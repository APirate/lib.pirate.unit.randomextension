﻿using Lib.Pirate.Unit.RandomExtension;
using Lib.Pirate.Unit.RandomExtension.StringProvider;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomExtensionTest
{
    class Program
    {
        static void Main(string[] args)
        {
            RandomString rs = new RandomString();
            //var members = rs.AddProviders(EStringProviders.SimpleChinese);
            //var cnProvider = members[EStringProviders.SimpleChinese][0] as SimpleChineseProvider;
            ////取出默认提供者
            //cnProvider.Providers.RemoveAll(q => true);
            ////添加常用字和不常用
            //var defined = cnProvider.AddProviders(ESimpleChineseCharType.Unique | ESimpleChineseCharType.Often);
            ////移除常用
            //cnProvider.RemoveProviders(defined[ESimpleChineseCharType.Often]);

            //添加英文字符数字
            rs.AddProviders(EStringProviders.Lowercase | EStringProviders.Uppercase);
            //数字，并且不要4
            //var sdd = rs.AddProviders(EStringProviders.SingleDigit);
            //sdd[EStringProviders.SingleDigit][0].AddExclude(4);

            do
            {
                Console.WriteLine("-----------------开始---------------------");

                Stopwatch timer = new Stopwatch();
                var max = rs.TotalMember;
                timer.Start();
                string[] rev = rs.NextMany(max);
                timer.Stop();

                Console.WriteLine(string.Join("\t", rev));

                Console.WriteLine("-----------------结束---------------------");
                Console.WriteLine("耗时{0}, 产生{1}个随机对象", timer.Elapsed.TotalMilliseconds, rev.Length); 
            }
            while (Console.ReadKey().Key != ConsoleKey.Escape);
        }
    }
}

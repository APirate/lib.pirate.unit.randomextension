﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.Pirate.Unit.RandomExtension
{
    public interface IRandom<T>
    {
        /// <summary>
        /// 随机数成员总个数
        /// </summary>
        int TotalMember { get; }

        T Next();

        T[] NextMany(int count, bool enableRepeat);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.Pirate.Unit.RandomExtension
{
    public class BaseProviderCollection<T> : IProvider<T>
    {
        public List<IProvider<T>> Providers
        {
            get
            {
                return this._providers;
            }
            set
            {
                this._providers = value;
            }
        }
        private List<IProvider<T>> _providers = new List<IProvider<T>>();
        private List<IProvider<T>> _providersNotNull
        {
            get
            {
                return this.Providers.Where(q => null != q).ToList();
            }
        }

        /// <summary>
        /// 定位到提供者及逻辑索引
        /// </summary>
        /// <param name="logicIndex"></param>
        /// <param name="providers"></param>
        /// <returns></returns>
        protected KeyValuePair<IProvider<T>, int> PositionLogicMember(int logicIndex)
        {
            IProvider<T> group = null;
            int innerIndex = -1;

            int before = 0;
            foreach (var item in this.Providers.Where(q => null != q))
            {
                if (logicIndex < before + item.FreeLength)
                {
                    innerIndex = logicIndex - before;
                    group = item;
                    break;
                }
                before += item.FreeLength;
            }

            return new KeyValuePair<IProvider<T>, int>(group, innerIndex);
        }

        /// <summary>
        /// 定位到提供者及实际索引
        /// </summary>
        /// <param name="realIndex">实际索引</param>
        /// <param name="providers"></param>
        /// <returns></returns>
        protected KeyValuePair<IProvider<T>, int> PositionMember(int realIndex)
        {
            IProvider<T> group = null;
            int innerIndex = -1;

            int before = 0;
            foreach (var item in this.Providers.Where(q => null != q))
            {
                if (realIndex < before + item.Length)
                {
                    innerIndex = realIndex - before;
                    group = item;
                    break;
                }
                before += item.Length;
            }

            return new KeyValuePair<IProvider<T>, int>(group, innerIndex);
        }

        #region IProvider
        public int Length
        {
            get
            {
                return this._providersNotNull.Sum(q => q.Length);
            }
        }

        public int FreeLength
        {
            get
            {
                return this._providersNotNull.Sum(q => q.FreeLength);
            }
        }
        #region 排除
        public int[] GetExcludes()
        {
            List<int> rev = new List<int>();
            int before = 0;
            foreach (var item in this.Providers.Where(q => null != q))
            {
                rev.AddRange(item.GetExcludes());
                before += item.Length;
            }

            return rev.ToArray();
        }

        public void AddExclude(int exclude)
        {
            var location = PositionMember(exclude);
            location.Key.AddExclude(location.Value);
        }

        public void AddExclude(IEnumerable<int> excludes)
        {
            if (null == excludes) return;

            foreach (var item in excludes)
            {
                this.AddExclude(item);
            }
        }

        public void RemoveExclude(int exclude)
        {
            var location = PositionMember(exclude);
            location.Key.RemoveExclude(location.Value);
        }

        public void RemoveExclude(IEnumerable<int> excludes)
        {
            if (null == excludes) return;

            foreach (var item in excludes)
            {
                this.RemoveExclude(item);
            }
        }

        public void RestExcludes()
        {
            foreach (var item in this.Providers.Where(q => null != q))
            {
                item.RestExcludes();
            }
        }
        #endregion 排除


        public T this[int index]
        {
            get 
            {
                var location = PositionLogicMember(index);
                return location.Key[location.Value];
            }
        }

        public T Pop(int index)
        {
            var location = PositionLogicMember(index);
            return location.Key.Pop(location.Value);
        }

        /// <summary>
        /// 深度复制
        /// </summary>
        /// <returns></returns>
        public virtual IProvider<T> DeepClone()
        {
            if (null == this) return null;

            var rev = this.MemberwiseClone() as BaseProviderCollection<T>;
            rev.Providers = new List<IProvider<T>>();
            rev.Providers.AddRange(this._providersNotNull.Select(q => q.DeepClone()));

            return rev;
        }
        #endregion IProvider
    }
}

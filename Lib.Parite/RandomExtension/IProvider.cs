﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.Pirate.Unit.RandomExtension
{
    /// <summary>
    /// 随机数提供者
    /// </summary>
    public interface IProvider<T>
    {
        /// <summary>
        /// 可用成员长度
        /// </summary>
        int FreeLength { get; }

        /// <summary>
        /// 原始长度
        /// </summary>
        int Length { get; }

        #region 排除
        /// <summary>
        /// 获取被排除的对象索引
        /// </summary>
        /// <returns></returns>
        int[] GetExcludes();

        /// <summary>
        /// 添加排除对象
        /// </summary>
        /// <param name="exclude"></param>
        void AddExclude(int exclude);

        /// <summary>
        /// 添加排除对象
        /// </summary>
        /// <param name="exclude"></param>
        void AddExclude(IEnumerable<int> exclude);

        /// <summary>
        /// 移除排除对象
        /// </summary>
        /// <param name="exclude"></param>
        void RemoveExclude(int exclude);

        /// <summary>
        /// 移除排除对象
        /// </summary>
        /// <param name="exclude"></param>
        void RemoveExclude(IEnumerable<int> exclude);

        /// <summary>
        /// 重置排除对象
        /// </summary>
        void RestExcludes();
        #endregion 排除

        /// <summary>
        /// 根据逻辑索引获取对象
        /// </summary>
        /// <param name="index"></param>
        T this[int index] { get; }

        /// <summary>
        /// 根据逻辑索引获取对象并弹出它（即插入到排除集）
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        T Pop(int index);

        /// <summary>
        /// 深度复制
        /// </summary>
        /// <returns></returns>
        IProvider<T> DeepClone();
    }
}

﻿/*==========================================================================================================
 * Description  : 
 * 
 * Operation    : huyong
 * Email        : 793912707@qq.com
 * Create Time  : 2015/5/21 11:35:13
 * =======================================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Lib.Pirate.Unit.RandomExtension.StringProvider;
using System.ComponentModel;
using Lib.Pirate.CSharp.SystemLib;

namespace Lib.Pirate.Unit.RandomExtension
{
    /*=====================================================================================================
     * Description  : 返回结果为字符串的随机数生成器。使用如下：
     *      RandomString rs = new RandomString();
     *      var members = rs.AddMembers(EStringProviders.Lowercase | EStringProviders.SingleDigit); //设置要添加的随机数候选集
     *      members[EStringProviders.Lowercase][0].Exclude = new SortedSet<int>() { 0, 25}; //设置要排除的索引项
     *      var rands = rs.NextMany(rs.TotalFreeMember); //所有项重新随机排列
     * Operation    : huyong
     * Email        : 793912707@qq.com
     * Create Time  : 2015/5/21 11:35:13
     * ===================================================================================================*/
    public class RandomString : BaseRandom<string>
    {
        protected BaseProviderCollection<string> Providers = new BaseProviderCollection<string>();
        protected override IProvider<string> GetProvider()
        {
            return this.Providers;
        }

        /// <summary>
        /// 通过枚举，向随机数生成器添加成员。
        /// </summary>
        /// <param name="providers"></param>
        /// <returns></returns>
        public DefinedStringProviders AddProviders(EStringProviders providers)
        {
            var definds = new DefinedStringProviders();
            this.Providers.Providers.AddRange(definds[providers]);
            return definds;
        }

        public void RemoveProviders(IEnumerable<IProvider<string>> providers)
        {
            if (null == providers) return;
            foreach (var item in providers)
            {
                this.Providers.Providers.Remove(item);   
            }
        }
    }

    /*=====================================================================================================
    * Description  : 随机数类型分组枚举
    * 
    * Operation    : huyong
    * Email        : 793912707@qq.com
    * Create Time  : 2015/5/21 11:05:18
    * ===================================================================================================*/
    public enum EStringProviders
    {
        /// <summary>
        /// 自定义
        /// </summary>
        [Description("自定义")]
        Custom = 1,
        /// <summary>
        /// 数字0-9
        /// </summary>
        [Description("数字")]
        SingleDigit = 2,
        /// <summary>
        /// 小写字母
        /// </summary>
        [Description("小写字母")]
        Lowercase = 4,
        /// <summary>
        /// 大写字母
        /// </summary>
        [Description("大写字母")]
        Uppercase = 8,
        /// <summary>
        /// 简体中文
        /// </summary>
        [Description("简体中文")]
        SimpleChinese = 16
    }

    /// <summary>
    /// 已定义的StringProviders。用户可通过EStringProviders枚举变量获得具体的IProvider。
    /// </summary>
    public class DefinedStringProviders : BaseDefinedProviders<EStringProviders, string>
    {
        public DefinedStringProviders()
        {
            this.Providers = new Dictionary<EStringProviders, IProvider<string>>()
            {
                  {EStringProviders.SingleDigit, new SingleDigitProvider()}
                , {EStringProviders.Lowercase, new LowercaseProvider()}
                , {EStringProviders.Uppercase, new UppercaseProvider()}
                , {EStringProviders.Custom, new CustomProvider()}
                , {EStringProviders.SimpleChinese, new SimpleChineseProvider()}
            };
        }
    }
}

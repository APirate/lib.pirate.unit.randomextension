﻿using Lib.Pirate.CSharp.SystemLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.Pirate.Unit.RandomExtension
{
    public class BaseDefinedProviders<TEnum, TProvider>
        where TEnum: struct
    {
        protected Dictionary<TEnum, IProvider<TProvider>> Providers
        {
            get
            {
                return this._providers;
            }
            set
            {
                this._providers = value ?? new Dictionary<TEnum, IProvider<TProvider>>();
            }
        }
        private Dictionary<TEnum, IProvider<TProvider>> _providers = new Dictionary<TEnum, IProvider<TProvider>>();
        
        public BaseDefinedProviders()
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("传入的类型不是枚举");
            }

            this._eDefineds = EnumExtension.GetMembers<TEnum>();
        }

        private List<TEnum> _eDefineds;
        public List<IProvider<TProvider>> this[TEnum ep]
        {
            get
            {
                return EnumExtension.Decompression(ep, this._eDefineds).Select(q =>
                    {
                        return this._providers.Keys.Contains(q)
                            ? this._providers[q]
                            : null;
                    }).ToList();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.Pirate.Unit.RandomExtension.StringProvider
{
    public class SimpleChineseProvider_Often: BaseProviderCollection<string>
    {
        public SimpleChineseProvider_Often()
        {
            //16区到55区为常用字，且55区后5位（5590 - 5594）未编码。（B0A1 - D7F9）
            for (int i = 16; i < 56; i++)
            {
                this.Providers.Add(new AreaCodeProvider(i));
            }
            //排除55区后5位
            var excludes = new[] { 89, 90, 91, 92, 93};
            var area55 = this.Providers.Last() as AreaCodeProvider;
            area55.AddExclude(excludes);
            area55.OnExcludesCleared += (sender) => 
            {
                sender.AddExclude(excludes);
            };
        }
    }
}

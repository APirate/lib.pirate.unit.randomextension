﻿/************************************************************************************
 * 参考资料：
 *      1. GB2312区位表 -- http://www.mytju.com/classCode/tools/QuWeiMa_FullList.asp
 * 区位码表解析：
 *      1. 16进制编码从A1A1开始，即1区1位的编码为A1A1
 *      2. 汉字是从16区开始，87区结束。(B0 - F7)
 *      3. 16区到55区为常用字，且55区后5位（5590 - 5594）未编码。（B0A1 - D7F9）
 *      4. 56-87区为生僻字（B1A1 - F7FE）
 *      5. 1-9区为中文符号
 *      6. 每个区的位都是从A1开始FE结束，共94个。
 * 
 * **********************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.Pirate.Unit.RandomExtension.StringProvider
{
    /// <summary>
    /// 中文简体提供者(GB2312编码)。
    /// </summary>
    public class SimpleChineseProvider: BaseProviderCollection<string>
    {
        /// <summary>
        /// 构造函数，已默认添加常用字。
        /// </summary>
        public SimpleChineseProvider()
        { 
            this.AddProviders(ESimpleChineseCharType.Often);
        }

        public DefinedSimpleChineseProviders AddProviders(ESimpleChineseCharType providers)
        {
            var definds = new DefinedSimpleChineseProviders();
            this.Providers.AddRange(definds[providers]);
            return definds;
        }

        public void RemoveProviders(IEnumerable<IProvider<string>> providers)
        {
            if (null == providers) return;
            foreach (var item in providers)
            {
                this.Providers.Remove(item);
            }
        }
    }

    public enum ESimpleChineseCharType
    { 
        /// <summary>
        /// 常用字
        /// </summary>
        [Description("常用字")]
        Often = 1,
        /// <summary>
        /// 生僻字
        /// </summary>
        [Description("生僻字")]
        Unique = 2,
        /// <summary>
        /// 符号
        /// </summary>
        [Description("符号")]
        Mark = 4
    }

    public class DefinedSimpleChineseProviders : BaseDefinedProviders<ESimpleChineseCharType, string>
    {
        public DefinedSimpleChineseProviders()
        {
            this.Providers = new Dictionary<ESimpleChineseCharType, IProvider<string>>()
            {
                  {ESimpleChineseCharType.Often, new SimpleChineseProvider_Often()}
                , {ESimpleChineseCharType.Unique, new SimpleChineseProvider_Unique()}
                //, {ESimpleChineseCharType.Uppercase, new UppercaseProvider()}
            };
        }
    }
}

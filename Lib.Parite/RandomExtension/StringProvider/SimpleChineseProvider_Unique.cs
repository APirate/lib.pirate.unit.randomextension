﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.Pirate.Unit.RandomExtension.StringProvider
{
    public class SimpleChineseProvider_Unique: BaseProviderCollection<string>
    {
        public SimpleChineseProvider_Unique()
        {
            //56-87区为生僻字（B1A1 - F7FE）
            for (int i = 56; i < 88; i++)
            {
                this.Providers.Add(new AreaCodeProvider(i));
            }
        }
    }
}

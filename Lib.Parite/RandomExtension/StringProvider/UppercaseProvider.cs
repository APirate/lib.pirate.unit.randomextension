﻿/*==========================================================================================================
 * Description  : 
 * 
 * Operation    : huyong
 * Email        : 793912707@qq.com
 * Create Time  : 2015/5/21 16:41:04
 * =======================================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.Pirate.Unit.RandomExtension.StringProvider
{
    /*=====================================================================================================
     * Description  : 
     * 
     * Operation    : huyong
     * Email        : 793912707@qq.com
     * Create Time  : 2015/5/21 16:41:04
     * ===================================================================================================*/
    public class UppercaseProvider : BaseProvider<string>
    {
        protected override string ElementAt(int realIndex)
        {
            return ((char)('A' + realIndex)).ToString();
        }

        public override int Length
        {
            get { return 26; }
        }
    }
}

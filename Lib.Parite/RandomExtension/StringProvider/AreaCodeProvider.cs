﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.Pirate.Unit.RandomExtension.StringProvider
{
    /// <summary>
    /// 区位码提供者。提供Exclude的时候，位请记得减1.
    /// </summary>
    public class AreaCodeProvider: BaseProvider<string>
    {
        public AreaCodeProvider(){}
        public AreaCodeProvider(int area)
            :this()
        {
            this.Area = area;
        }

        /// <summary>
        /// 区
        /// </summary>
        public int Area
        {
            get
            {
                return this._area;
            }
            set
            {
                if (1 > value
                    || 94 < value)
                {
                    throw new ArgumentException("区码必须在1到94之间。");
                }
                this._area = value;
                this._areaCode = Convert.ToByte((value + AreaCodeStart - 1).ToString());
            }
        }
        private int _area = 1;

        //区域的2进制编码
        private byte _areaCode;

        /// <summary>
        /// 区开始位置 161 (A1)
        /// </summary>
        private const int AreaCodeStart = 161;
        /// <summary>
        /// 位开始位置161 (A1)
        /// </summary>
        private const int PlaceStart = 161;

        protected override string ElementAt(int index)
        {
            //将"位"转化为区域码的2进制
            index = PlaceStart + index - 1;

            //将区位码的2进制编码转化为汉字
            byte place = Convert.ToByte(index.ToString());
            string rev = Encoding.GetEncoding("gb2312").GetString(new[] { this._areaCode, place });

            return rev;
        }

        public override int Length
        {
            get 
            {
                return 94;
            }
        }

        public delegate void OnExcludesClearedHander(AreaCodeProvider sender);
        /// <summary>
        /// AreaHeader初始化时执行
        /// </summary>
        public event OnExcludesClearedHander OnExcludesCleared;
       
        /// <summary>
        /// 初始化区位码
        /// </summary>
        private void InitAreaCode()
        {
            if (null != OnExcludesCleared)
            {
                OnExcludesCleared(this);
            }
        }

        public override void RestExcludes()
        {
            base.RestExcludes();
            InitAreaCode();
        }
    }
}

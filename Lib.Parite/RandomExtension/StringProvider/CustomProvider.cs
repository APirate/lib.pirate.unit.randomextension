﻿/*==========================================================================================================
 * Description  : 
 * 
 * Operation    : huyong
 * Email        : 793912707@qq.com
 * Create Time  : 2015/5/21 16:56:48
 * =======================================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.Pirate.Unit.RandomExtension.StringProvider
{
    /*=====================================================================================================
     * Description  : 随机数提供者，用户可设置候选集
     * 
     * Operation    : huyong
     * Email        : 793912707@qq.com
     * Create Time  : 2015/5/21 16:56:48
     * ===================================================================================================*/
    public class CustomProvider : BaseProvider<string>
    {
        public string[] Members;

        protected override string ElementAt(int realIndex)
        {
            string t = null == Members
                ?  null
                : Members[realIndex];
            return t;
        }

        public override int Length
        {
            get 
            {
                return (null == Members) ? 0 : Members.Length;
            }
        }
    }
}

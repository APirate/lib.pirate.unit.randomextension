﻿/*==========================================================================================================
 * Description  : 
 * 
 * Operation    : huyong
 * Email        : 793912707@qq.com
 * Create Time  : 2015/5/21 11:33:45
 * =======================================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.Pirate.Unit.RandomExtension.StringProvider
{
    /*=====================================================================================================
     * Description  : 数字0-9 随机数提供者
     * 
     * Operation    : huyong
     * Email        : 793912707@qq.com
     * Create Time  : 2015/5/21 11:33:45
     * ===================================================================================================*/
    public class SingleDigitProvider : BaseProvider<string>
    {
        protected override string ElementAt(int index)
        {
              return index.ToString();
        }

        public override int Length
        {
            get { return 10; }
        }
    }
}

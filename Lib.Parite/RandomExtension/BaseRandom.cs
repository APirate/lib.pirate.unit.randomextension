﻿/*==========================================================================================================
 * Description  : 
 * 
 * Operation    : huyong
 * Email        : 793912707@qq.com
 * Create Time  : 2015/5/22 17:40:50
 * =======================================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lib.Pirate.CSharp.SystemLib;

namespace Lib.Pirate.Unit.RandomExtension
{
    /*=====================================================================================================
     * Description  : 
     * 
     * Operation    : huyong
     * Email        : 793912707@qq.com
     * Create Time  : 2015/5/22 17:40:50
     * ===================================================================================================*/
    public abstract class BaseRandom<T>: IRandom<T>
    {
        protected abstract IProvider<T> GetProvider();
        private IProvider<T> _providers
        {
            get
            {
                return this.GetProvider();
            }
        }

        public int TotalMember
        {
            get {
                return this._providers.FreeLength;
            }
        }

        public T Next()
        {
            int randNum = GetRandomNum(this._providers);
            return this._providers[randNum];
        }

        /// <summary>
        /// 获取多个随机对象
        /// </summary>
        /// <param name="count">个数</param>
        /// <param name="enableRepeat">是否允许重复，默认不允许。</param>
        /// <returns></returns>
        public T[] NextMany(int count, bool enableRepeat = false)
        {
            T[] rev = new T[count];
            if (enableRepeat)
            {
                for (int i = 0; i < count; i++)
                {
                    rev[i] = this.Next();
                }
            }
            else
            {
                var providers = this._providers.DeepClone();
                for (int i = 0; i < count; i++)
                {
                    int randNum = GetRandomNum(providers);
                    rev[i] = providers.Pop(randNum);
                }
            }

            return rev;
        }

        private Random _mathRandom = new Random(unchecked((int)DateTime.Now.Ticks));
        /// <summary>
        /// 获取一个随机数
        /// </summary>
        /// <param name="providers"></param>
        /// <returns></returns>
        protected int GetRandomNum(IProvider<T> providers)
        {
            int max = 0;
            if (null == providers
                || 0 == (max = providers.FreeLength))
            { 
                throw new Exception("随机成员获取失败，可能是候选集未设置或已用尽。");
            }

            return _mathRandom.Next(0, max);
        }
    }
}

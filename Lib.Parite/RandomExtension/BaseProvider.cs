﻿/*==========================================================================================================
 * Description  : 
 * 
 * Operation    : huyong
 * Email        : 793912707@qq.com
 * Create Time  : 2015/5/21 16:16:28
 * =======================================================================================================*/

using Lib.Pirate.Unit.RandomExtension.StringProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.Pirate.Unit.RandomExtension
{
    /*=====================================================================================================
     * Description  : 随机数提供者模板
     * 
     * Operation    : huyong
     * Email        : 793912707@qq.com
     * Create Time  : 2015/5/21 16:16:28
     * ===================================================================================================*/
    public abstract class BaseProvider<T>: IProvider<T>
    {
        private SortedSet<int> _exclude = new SortedSet<int>();

        /// <summary>
        /// 根据逻辑索引获取对象
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public T this[int index]
        {
            get
            {
                index = ToRealIndex(index);
                return this.ElementAt(index);
            }
        }

        /// <summary>
        /// 根据逻辑索引获取对象并弹出它（即插入到排除集）
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public T Pop(int index)
        {
            index = ToRealIndex(index);
            this.AddExclude(index);

            return this.ElementAt(index);
        }

        /// <summary>
        /// 根据实际索引获取对象
        /// </summary>
        /// <param name="realIndex"></param>
        /// <returns></returns>
        protected abstract T ElementAt(int realIndex);

        /// <summary>
        /// 将逻辑索引转换为真实索引
        /// </summary>
        /// <param name="logicIndex"></param>
        /// <returns></returns>
        protected virtual int ToRealIndex(int logicIndex)
        {
            if (0 == this._exclude.Count) return logicIndex;
            if (0 >= this.FreeLength)
            {
                throw new Exception("随机成员获取失败，可能是候选集未设置或已用尽。");
            }

            int before = 0;
            foreach (var exItem in this._exclude)
            {
                if (exItem <= logicIndex + before)
                    before++;
                else break;
            }

            return logicIndex + before;
        }

        #region IProvider
        /// <summary>
        /// 可使用对象长度（原始长度 - 排除）
        /// </summary>
        public virtual int FreeLength
        {
            get
            {
                return this.Length - this._exclude.Count;
            }
        }

        /// <summary>
        /// 原始长度
        /// </summary>
        public abstract int Length
        {
            get;
        }

        #region 排除
        public virtual int[] GetExcludes()
        {
            return this._exclude.ToArray();
        }

        public virtual void AddExclude(int exclude)
        {
            this._exclude.Add(exclude);
        }

        public virtual void AddExclude(IEnumerable<int> excludes)
        {
            if (null == excludes) return;

            foreach (var item in excludes)
	        {
                this._exclude.Add(item);
	        }            
        }

        public virtual void RemoveExclude(int exclude)
        {
            this._exclude.Remove(exclude);
        }

        public virtual void RemoveExclude(IEnumerable<int> excludes)
        {
            this._exclude.RemoveWhere(q => excludes.Contains(q));
        }

        public virtual void RestExcludes()
        {
            this._exclude.Clear();
        }
        #endregion 排除

        /// <summary>
        /// 深度复制
        /// </summary>
        /// <returns></returns>
        public virtual IProvider<T> DeepClone()
        {
            if (null == this) return null;

            //只需要创建一个this._exclude副本
            SortedSet<int> ex = this._exclude;
            this._exclude = new SortedSet<int>();
            IProvider<T> rev = this.MemberwiseClone() as IProvider<T>;
            this._exclude = ex;
            rev.AddExclude(ex);

            return rev;
        }
        #endregion IProvider


    }
}
